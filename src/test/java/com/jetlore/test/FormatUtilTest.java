package com.jetlore.test;

import com.jetlore.test.model.Entity;
import com.jetlore.test.model.Item;
import com.jetlore.test.model.Link;
import com.jetlore.test.model.TweetUser;
import com.jetlore.test.utils.FormatUtil;
import org.junit.Test;

import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FormatUtilTest {

	private static final String EXPECTED = "<strong>Obama</strong> visited <strong>Facebook</strong> headquarters: <a href=”http://bit.ly/xyz”>http://bit.ly/xyz </a> @ <a href=”http://twitter.com/elversatile”>elversatile</a>";

	@Test
	public void test() {
		final String input = "Obama visited Facebook headquarters: http://bit.ly/xyz @elversatile";
		TreeSet<Item> items = new TreeSet<>();
		items.add(new Entity(0, 5));
		items.add(new Entity(14, 22));
		items.add(new TweetUser(55, 67));
		items.add(new Link(37, 54));

		final String formatResult = FormatUtil.format(input, items);

		System.out.println(formatResult);
		assertEquals(EXPECTED, formatResult);
	}
}
