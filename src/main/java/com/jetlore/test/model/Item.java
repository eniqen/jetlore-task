package com.jetlore.test.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.function.Function;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
@Getter
@EqualsAndHashCode
@RequiredArgsConstructor
public abstract class Item implements Comparable<Item> {

	private final int x;
	private final int y;

	public abstract Function<String, String> format();

	@Override
	public int compareTo(Item that) {
		if (this.x < that.x || this.x == that.x && this.y < that.y) {
			return -1;
		} else {
			return 1;
		}
	}
}
