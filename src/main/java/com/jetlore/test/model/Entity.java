package com.jetlore.test.model;

import java.util.function.Function;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */

public class Entity extends Item {

	public Entity(int x, int y) {
		super(x, y);
	}

	@Override
	public Function<String, String> format() {
		return text -> String.format("<strong>%s</strong>", text);
	}
}
