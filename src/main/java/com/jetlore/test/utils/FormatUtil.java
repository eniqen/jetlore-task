package com.jetlore.test.utils;

import com.jetlore.test.model.Item;
import lombok.val;

import java.util.TreeSet;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public final class FormatUtil {

	private FormatUtil() {
	}

	public static String format(String text, TreeSet<Item> items) {

		val builder = new StringBuilder(text);
		final int[] index = {0};

		items.forEach(item -> {

			val start = item.getX() + index[0];
			val end = item.getY() + index[0];

			val cutValue = builder.substring(start, end);
			val wrapperValue = item.format().apply(cutValue);

			builder.replace(start, end, wrapperValue);

			index[0] += wrapperValue.length() - cutValue.length();
		});

		return builder.toString();
	}
}
