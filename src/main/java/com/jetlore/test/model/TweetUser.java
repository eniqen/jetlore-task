package com.jetlore.test.model;

import java.util.function.Function;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class TweetUser extends Item {

	public TweetUser(int x, int y) {
		super(x, y);
	}

	@Override
	public Function<String, String> format() {
		return text -> {
			final String name = text.substring(1);
			return String.format("@ <a href=”http://twitter.com/%s”>%s</a>", name, name);
		};
	}
}
