package com.jetlore.test.model;

import java.util.function.Function;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Link extends Item {

	public Link(int x, int y) {
		super(x, y);
	}

	@Override
	public Function<String, String> format() {
		return text -> String.format("<a href=”%s”>%s </a>", text, text);
	}
}
